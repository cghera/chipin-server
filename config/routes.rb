Rails.application.routes.draw do
  devise_for :users
  get 'home/index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  get '/redirect', to: 'redirect#index', as: 'redirect'
  namespace :api do
    namespace :v1 do
      resources :users, :only => [:create]
      resources :contacts
      resources :events
      resources :tokens, :only => [:create, :destroy]
      post '/events/:id/chipins', to: 'events#chipin', as: 'chipin_create'
      post '/events/:id/members', to: 'events#add_member', as: 'member_add'
      post '/events/:id/products', to: 'events#add_product', as: 'product_add'
      post '/events/:id/products/:part_number_key/votes', to: 'events#vote_product', as: 'product_vote'
      put '/users', to: 'users#update', as: 'user_update'
      get '/upcoming', to: 'upcoming#index', as: 'upcoming_index'
    end
  end
end
