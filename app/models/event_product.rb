class EventProduct < ActiveRecord::Base
  belongs_to :event
  has_many :product_votes

  def from_server
    json = `/home/rails/decrypt.php https://m-api.emag.ro/v0.0/product/listing?part_number_keys[]=#{part_number_key}`
    require 'json'
    response = JSON.parse json
    if response
      response["data"][0][:votes] = product_votes.count
      return response["data"][0]
    end
    return nil
  end
end
