class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable #, :validatable
  before_save :ensure_authentication_token

  has_many :contacts
  has_many :events

  def as_json(options={})
    fields = super(:only => [:id, :name, :phone_number, :birthday])
    fields[:is_member] = true
    fields[:avatar] = File.exists?("/home/rails/public/avatars/#{id}.jpg") ? "http://chipin.cristi.me/avatars/#{id}.jpg" : "http://icons.iconarchive.com/icons/graphicloads/100-flat/256/contact-icon.png"
    fields
  end

  def save_avatar(avatar)
    require 'base64'
    binary_data = Base64.decode64(avatar)
    File.open("/home/rails/public/avatars/#{id}.jpg", "wb") do |f|
      f.write binary_data
    end
  end
 
  def ensure_authentication_token
    if authentication_token.blank?
      self.authentication_token = generate_authentication_token
    end
  end
 
  private
  
  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(authentication_token: token).first
    end
  end

end
