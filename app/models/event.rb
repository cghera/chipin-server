class Event < ActiveRecord::Base
  belongs_to :user
  belongs_to :target, class_name: 'User', foreign_key: 'target_id'
  has_many :event_members
  has_many :event_chipins
  has_many :event_products

  def self.events_for_user(user)
    events = user.events.to_a
    events_invited = EventMember.where(phone_number: user.phone_number).all.each do |em|
      events << em.event
    end
    events.sort_by! do |e| 
      [e.target.birthday.month, e.target.birthday.day]
    end
  end

  def as_json(options={})
    fields = super(:only => [:id, :topic])
    fields[:target] = target.as_json
    fields[:user] = user.as_json
    members = []
    event_members.each do |em|
      user = User.find_by_phone_number(em.phone_number)
      if user
        members << user.as_json
      else
        begin
          contact = Contact.find_by(phone_no: em.phone_number, user_id: em.inviter_id)
          members << contact.as_json
        rescue Exception => ex
        end
      end
    end
    fields[:members] = members
    fields[:chipped] = event_chipins.sum(:sum).round(2)
    fields[:products] = event_products.map do |product| product.from_server end.sort_by! do |p| p[:votes] end
    fields
  end
end
