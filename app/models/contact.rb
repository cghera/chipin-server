class Contact < ActiveRecord::Base
    belongs_to :user
    attr_accessor :associated_user

    def get_associated_user
        associated_user = User.find_by_phone_number(phone_no) unless associated_user
        associated_user
    end

    def as_json(options={})
        fields = super(:only => [:id, :name, :birthday])
        fields[:avatar] = "http://icons.iconarchive.com/icons/graphicloads/100-flat/256/contact-icon.png"
        fields[:phone_number] = phone_no
        if get_associated_user
          fields[:is_member] = true
          fields[:name] = get_associated_user.name
          fields[:birthday] = get_associated_user.birthday
        else
          fields[:is_member] = false
        end
        fields
    end
end
