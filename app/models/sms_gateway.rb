class SmsGateway
  API_ENDPOINT = "https://api.checkmobi.com/v1/sms/send"
  API_KEY = "B1E21E31-4EE3-4766-A492-E1FEF9BF93D4"

  def self.send_sms(options)
    require 'net/https'
    uri = URI(API_ENDPOINT)
    req = Net::HTTP::Post.new(uri, initheader = {'Content-Type' =>'application/json', 'Authorization' => API_KEY})
    #req.body = {text: "#{options[:from]} te invită la http://chipin.cristi.me/redirect", to: options[:to]}.to_json
    req.body = {text: "#{options[:from]} invites you to join him in creating an awesome surprise for #{options[:target]} on his birthday. Download Chipin app and find out more", to: options[:to]}.to_json
    http = Net::HTTP.new(uri.hostname, uri.port)
    http.use_ssl = true
    res = http.request(req)
  end
end
