class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  protect_from_forgery with: :null_session, if: Proc.new {|c| c.request.format.json? }
  before_filter :authenticate_user_from_token!

  private
  def authenticate_user_from_token!
    user_phone_no = params[:phone].presence
    user          = user_phone_no && User.find_by_phone_number(user_phone_no)
 
    # Notice how we use Devise.secure_compare to compare the token
    # in the database with the token given in the params, mitigating
    # timing attacks.
    if user # && Devise.secure_compare(user.authentication_token, params[:token])
      sign_in user, store: false
    end
  end

end
