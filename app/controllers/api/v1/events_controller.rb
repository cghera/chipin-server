class Api::V1::EventsController  < ApplicationController 
  #skip_before_filter :verify_authenticity_token
  respond_to :json

  def index
    if current_user then
      render :json => Event.events_for_user(current_user)
    else
      render :json => nil
    end
  end

  def show
    if not current_user
      render :json => nil
      return
    end
    event = Event.find(params[:id])
    if (event.user and event.user.phone_number == current_user.phone_number)
      render :json => event
    else
      is_member = !event.event_members.where(phone_number: current_user.phone_number).empty?
      render :json => is_member ? event : nil
    end
  end

  def chipin
    if current_user
      chipin = EventChipin.create!(event_id: params[:id].presence, sum: params[:sum], phone_number: current_user.phone_number)
      if chipin
        render :json => { success: true, id: chipin.id }
        return
      end
    end
    render :json => { success: false }
  end

  def add_member
    if current_user
      begin
        if EventMember.find_by(event_id: params[:id], phone_number: params[:phone_number]).empty? then
          render :json => { success: false, reason: "User is already participating" }
          return
        end
      rescue Exception => ex
      end
      membership = EventMember.create!(event_id: params[:id].presence, phone_number: params[:phone_number], inviter_id: current_user.id)
      if membership
        SmsGateway.send_sms(from: current_user.name, target: membership.event.target.name, to: params[:phone_number])
        render :json => { success: true, id: membership.id }
        return
      end
    end
    render :json => { success: false }
  end

  def add_product
    if current_user
      begin
        if EventProduct.find_by(event_id: params[:id], part_number_key: params[:part_number_key]).empty? then
          render :json => { success: false, reason: "Product is already present" }
          return
        end
      rescue Exception => ex
      end
      product = EventProduct.create!(event_id: params[:id].presence, part_number_key: params[:part_number_key])
      if product
        render :json => { success: true, id: product.id }
        return
      end
    end
    render :json => { success: false }
  end

  def vote_product
    if current_user
      begin
        product = EventProduct.find_by(event_id: params[:id], part_number_key: params[:part_number_key])
        if product then
          begin
            vote = ProductVote.find_by(phone_number: current_user.phone_number)
            if vote
              epid = vote.event_product_id
              vote.destroy
              if epid == product.id
                render :json => { success: true, voted: false }
                return
              end
            end
            vote = ProductVote.create!(event_product_id: product.id, phone_number: current_user.phone_number)
            if vote
              render :json => { success: true, voted: true }
              return
            else
              render :json => { success: false }
              return
            end
          rescue ActiveRecord::RecordNotFound => ex
            vote = ProductVote.create!(event_product_id: product.id, phone_number: current_user.phone_number)
            if vote
              render :json => { success: true, voted: true }
              return
            else
              render :json => { success: false }
              return
            end
          rescue Exception => e
          end
        end
      rescue Exception => ex
      end
    end
    render :json => { success: false }
  end
  
  def create
    if current_user
      data = {
          user_id: current_user.id,
          target_id: params[:phone_number].presence,
	  topic: params[:topic].presence,
      }
      event = Event.create!(data)
      if event
        render :json => { success: true, id: event.id }
      else
        render :json => { success: false }
      end
    else
      render :json => nil
    end
  end
  
  def destroy
  end  

end


