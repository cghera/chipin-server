class Api::V1::ContactsController  < ApplicationController 
  #skip_before_filter :verify_authenticity_token
  respond_to :json

  def index
    if current_user then
      contacts = current_user.contacts
      render :json => contacts
    else
      render :json => nil
    end
  end
  
  def create
    if current_user
      if Contact.where(:user_id => current_user.id, phone_no: params[:phone_number].presence).exists?
        render :json => { success: false }
        return
      end
      data = {
          user_id: current_user.id,
          phone_no: params[:phone_number].presence,
	  name: params[:name].presence,
          birthday: params[:birthday]
      }
      contact = Contact.create(data)
      if contact
        render :json => { success: true, id: contact.id }
      else
        render :json => { success: false }
      end
    else
      render :json => nil
    end
  end
  
  def destroy
  end  

end

