class Api::V1::UsersController  < ApplicationController 
  #skip_before_filter :verify_authenticity_token
  respond_to :json

  def index
  end
  
  def create
    user = User.find_by_phone_number(params[:phone_number].presence)
    if user
      render :json => { success: false, reason: "The number already has an account associated" }
      return
    end
    user = User.create!(name: params[:name].presence, birthday: params[:birthday].presence, phone_number: params[:phone_number])
    if user
      user.ensure_authentication_token
      avatar = params[:avatar].presence
      if avatar
        user.save_avatar avatar
      end
      render :json => { success: true, id: user.id, token: user.authentication_token }
      return
    end
    render :json => { success: false }
  end
  
  def update
    user = current_user
    if not user
      render :json => { success: false, reason: "Not logged in" }
      return
    end
    if user
      avatar = params[:avatar].presence
      if avatar
        user.save_avatar avatar
      end
      render :json => { success: true, id: user.id, token: user.authentication_token }
      return
    end
    render :json => { success: false }
  end

  def destroy
  end  

end


