class Api::V1::UpcomingController  < ApplicationController 
  #skip_before_filter :verify_authenticity_token
  respond_to :json

  def index
    if current_user then
      today = Date.today
      contacts = current_user.contacts
      sortable_contacts = contacts.map do |contact|
        bday = contact.birthday
        bday = bday.change(year: today.year)
        bday = bday.change(year: bday.year + 1) if bday < today
        return { birthday: bday, contact: contact }
      end
      sortable_contacts.sort_by! do |sc| sc[:birthday] end
      render :json => nil
      #render :json => sortable_contacts.map { |sc| sc[:contact] }
      #render :json => sortable_contacts
    else
      render :json => nil
    end
  end

end
