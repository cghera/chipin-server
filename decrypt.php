#!/usr/bin/env php
<?php
function aes256Decrypt($key, $data) {
    if(32 !== strlen($key)) $key = hash('SHA256', $key, true);
    $data = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, str_repeat("\0", 16));
    $padding = ord($data[strlen($data) - 1]);
    return substr($data, 0, -$padding);
}
echo aes256Decrypt('QuidJeibimashog0', file_get_contents($argv[1]));
