class CreateProductVotes < ActiveRecord::Migration
  def change
    create_table :product_votes do |t|
      t.string :phone_number
      t.integer :event_product_id

      t.timestamps null: false
    end
  end
end
