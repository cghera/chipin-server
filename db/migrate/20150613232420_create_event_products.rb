class CreateEventProducts < ActiveRecord::Migration
  def change
    create_table :event_products do |t|
      t.integer :event_id
      t.string :part_number_key

      t.timestamps null: false
    end
    add_index :event_products, :event_id
    add_index :event_products, :part_number_key
  end
end
