class CreateEventMembers < ActiveRecord::Migration
  def change
    create_table :event_members do |t|
      t.integer :event_id
      t.integer :user_phone
      t.integer :inviter_id

      t.timestamps null: false
    end

    add_index :event_members, :user_phone, unique: true
  end
end
