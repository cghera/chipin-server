class AddAuthenticationTokenToUsers < ActiveRecord::Migration
  def change
    add_column :users, :authentication_token, :string
    add_column :users, :phone_number, :string
    add_column :users, :name, :string
    add_column :users, :birthday, :date

    add_index :users, :authentication_token
    add_index :users, :phone_number, unique: true
  end
end
