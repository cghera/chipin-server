class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer :user_id
      t.integer :target_id
      t.string :topic

      t.timestamps null: false
    end
  end
end
