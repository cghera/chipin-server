class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.integer :user_id
      t.string :phone_no
      t.string :name
      t.date :birthday
      t.integer :rel_id

      t.timestamps null: false
    end
  end
end
