class CreateEventChipins < ActiveRecord::Migration
  def change
    create_table :event_chipins do |t|
      t.integer :event_id
      t.integer :phone_number
      t.float :sum

      t.timestamps null: false
    end
    add_index :event_chipins, :event_id
    add_index :event_chipins, :phone_number
  end
end
